# An example wordpress based website
This is the simplest way to bring up a wordpress website, using WBITT's wordpress docker image. You do not need anything fancy in this repository.

The `wbitt/wordpress` image has the following features:
* Small memory footprint (Nginx + PHP-FPM)
* Very fast!
* Ability to deploy a wordpress website in *immutable* way.
* Plugins and themes are specified as configuration - which prevents the site admin to go on an "install spree"
* Ability to copy and install user provided plugins and themes into the docker image
* Can run any number of custom scripts - after calling the official wordpress `docker-entrypoint.sh`; simply place your scripts under `docker-entrypoint.d/`
* Use your GIT user and GIT TOKEN for the image to pull themes and plugins from private repositories that you have access to.
* The only piece of data, you need to keep persistent - and take backup of - is the `wp-content/uploads` directory.


The code repository for the `wbitt/wordpress` docker image is here: [https://gitlab.com/wbitt/docker/wordpress](https://gitlab.com/wbitt/docker/wordpress) 

## Location of this image:

```
docker pull wbitt/wordpress:5.9.3-php-8.1-nginx-fpm
```

Tags: 
* `5.9.3-php-8.1-nginx-fpm` - NGINX + PHP-FPM based image - **recommended**
* `latest` - Apache based image - un-maintained - soon to be decommissioned


## Documentation / examples:
* [docs/development-setup-on-docker.md](docs/development-setup-on-docker.md)
* [docs/production-setup-on-docker.md](docs/production-setup-on-docker.md)
* [docs/production-setup-on-kubernetes.md](docs/production-setup-on-kubernetes.md)
