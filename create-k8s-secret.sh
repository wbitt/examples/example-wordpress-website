#!/bin/sh

# Note: Don't use colon (:) in any of the echo commands in the script,
#         because gitlab-ci.yml goes crazy when it sees it. 

echo
echo "Usage - $0 <namespace>  <site-name> <secret-name>  <path-and-name-of-secrets-file_ENV_FILE> "
if [ -z "$1" ]; then
  echo "Please provide the name of NAMESPACE to use e.g. (prod). Exiting ..."
  exit 1
else
  NAMESPACE=$1
fi

if [ -z "$2" ]; then
  echo "Please provide the name of the website this secret is being creatad for."
  echo "  e.g. example.com"
  echo " Exiting ..."
  exit 1
else
  K8S_SITE_NAME=$(echo $2 | tr '.' '-')
  SITE_ID=${SITE_ID:-${K8S_SITE_NAME}}
fi

if [ -z "$3" ]; then
  echo "Please provide the name of the secret to create."
  echo "  Allowed characters: all lowercase, alpha-numeric, hyphen (-) "
  echo "e.g. credentials-example-com"
  echo " Exiting ..."
  exit 1
else
  K8S_SECRET_NAME=$3
fi

if [ -z "$4" ]; then
  echo "Please provide the full path of the secrets file (ENV_FILE) to use. Exiting ..."
  exit 1
else
  ENV_FILE=$4
fi


############################ END - USER configurable section ###########

if [ ! -r ${ENV_FILE} ]; then
  echo "File ${ENV_FILE} is not readable . Exiting ..."
  exit 1
fi

echo
echo "Processing secrets file - ${ENV_FILE}..."

NAMESPACE_OPTION="--namespace=${NAMESPACE}"

K8S_COMMAND="kubectl ${NAMESPACE_OPTION} create secret generic ${K8S_SECRET_NAME} "

TEMP_FILE=$(mktemp)
# Clean up the file by removing comments and empty lines.
#   Then, only include lines with '=' present in them. 
egrep -v '\#|^$' ${ENV_FILE} | grep '=' > ${TEMP_FILE}

# Build the full command to create secret.

echo "Ready to build the K8s_COMMAND by using a while loop ..."

TEMP_ENV_YAML_FILE=site-deployment.template-env.yaml

while IFS= read -r LINE; do
  VARIABLE_NAME=$(echo ${LINE} | cut -d '=' -f1)
  K8S_COMMAND="${K8S_COMMAND} --from-literal=${LINE}"

  echo "Adding variable -  ${VARIABLE_NAME} ..."

  # The following creates a temporary file, 
  #   which is used to create a section of deployment file, requiring secrets.
  # Doing it this way ensures that the secrets section gets only those variables
  #   which this script finds in the container's environment.
  # The spaces in the following few lines is VERY important.
  echo "Adding variable to yaml file - ${TEMP_ENV_YAML_FILE}"
  echo "        - name: ${VARIABLE_NAME}"            >> ${TEMP_ENV_YAML_FILE}
  echo "          valueFrom:"                        >> ${TEMP_ENV_YAML_FILE}
  echo "            secretKeyRef:"                   >> ${TEMP_ENV_YAML_FILE}
  echo "              name: credentials-${SITE_ID}"  >> ${TEMP_ENV_YAML_FILE}
  echo "              key: ${VARIABLE_NAME}"         >> ${TEMP_ENV_YAML_FILE}  

done < ${TEMP_FILE}

echo

# echo "DEBUG: complete command: ${K8S_COMMAND}"


# source ${ENV_FILE} 

echo "Deleting secret - ${K8S_SECRET_NAME} if already exists in namespace - ${NAMESPACE} ..."
echo

kubectl ${NAMESPACE_OPTION} delete secret ${K8S_SECRET_NAME} || true

echo
echo "Creating secret - ${K8S_SECRET_NAME} - inside namespace - ${NAMESPACE} ..."
echo

echo "Done."
echo
# Execute the command we built in the while loop above.


${K8S_COMMAND}

echo

