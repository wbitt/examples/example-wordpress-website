# This is the environment file mainly used for wordpress nginx-php-fpm container.
#   It may contain sensitive information, which cannot simply be
#   put in the docker-compose.yaml file.


# Change the WORDPRESS_DB_HOST to the exact IP/DNS-Name where your DB is located.
# - If it is part of this docker-compose stack, you can leave it to db.local
# - If it is elsewhere, then chnage it to exact IP/DNS of that location.

WORDPRESS_DB_HOST=db
WORDPRESS_DB_NAME=wordpress
WORDPRESS_DB_USER=wordpress
WORDPRESS_DB_PASSWORD=wordpress
WORDPRESS_TABLE_PREFIX=wp_

# You must always set TimeZone for your container.
# The format follows the directory layout in "/usr/share/zoneinfo/" on any linux system.
#
TZ=Europe/London


# This is important to set. 
# If this is skipped, NGINX_HOST will get default value of 'localhost'.
NGINX_HOST=example.com

# This is important to set. 
# If this is skipped, NGINX_PORT will get default value of '80'.
# In some cases you may want to run NGINX on a different port,
#   such as instances when you have a cache server (varnish) 
#   running in front of nginx .
# In that case set it to 8780 (ASCII code for W=87, P=80 -> WP=8780 )
# NGINX_PORT=8780
NGINX_PORT=80


# This variable defines the location of the nginx template file for "default.conf"
# If you don't specify this, it will be set to /etc/nginx/templates/default.conf.template
NGINX_CONFIG_TEMPLATE_FILE=/etc/nginx/templates/default.conf.template

# Set 'gzip' = 'on'|'off' in main nginx.conf. 
#   Turning it off reduced load on web server, and is better for the web-server.
# Default is 'off'.
NGINX_GZIP=off


# The following two variables are used by php-fpm container to run as this user.
#   If you don't specify this, then the default "www-data" with uid "33",
#   will be used by the php-fpm container.
# On a DEV PC, you can use the UID of your local user as WEB_RUN_UID, 
#   instead of 1000.

WEB_RUN_USERNAME=web
WEB_RUN_UID=1000

# The following is only used if you are using some private repositories
#   from your GIT provider.
#
# GIT_USER=yourgit-provider-accountname
# GIT_TOKEN=yourgit-provider-token

# The following DELETE_* variables are used to delete ALL themes,
#   and ALL plugins when set to true. This should be relatively 
#   harmless operation, because you should have themes and plugins
#   either part of this repo, or listed in the download*.list files.
# In worst case situation, the wordpress admin panel is always available,
#   which can be used to install a theme manually.
# Once happy with cleaning up, set these two variables back to "false",
#   and restart the docker-compose stack.

DELETE_ALL_THEMES=false
DELETE_ALL_PLUGINS=false
DELETE_WEB_DOCUMENT_ROOT=false

