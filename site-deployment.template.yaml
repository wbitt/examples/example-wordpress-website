# * We need the WORDPRESS_* environment variables as a secret. 
# * These WORDPRESS_* env vars will be used by wordpress deployment.

apiVersion: apps/v1
kind: Deployment
metadata:
  name: SITE_ID
  labels:
    app: SITE_ID
    tier: frontend
    apptype: wordpress
spec:
  selector:
    matchLabels:
      app: SITE_ID
      tier: frontend
      apptype: wordpress
  template:
    metadata:
      labels:
        app: SITE_ID
        tier: frontend
        apptype: wordpress
    spec:
      containers:
      - name: SITE_ID
        image: eu.gcr.io/GCP_PROJECT_ID/GCR_IMAGE_NAME:IMAGE_TAG
        imagePullPolicy: Always

        env:
        - name: TZ
          value: "Europe/London"
          # TZ is used by various system processes,
          #   including PHP, 
          #   which uses this if "date.timezone" is not defined in php.ini file.


        - name: WORDPRESS_DB_HOST
          valueFrom:
            secretKeyRef:
              name: credentials-SITE_ID
              key: WORDPRESS_DB_HOST
        - name: WORDPRESS_DB_NAME
          valueFrom:
            secretKeyRef:
              name: credentials-SITE_ID
              key: WORDPRESS_DB_NAME
        - name: WORDPRESS_DB_USER
          valueFrom:
            secretKeyRef:
              name: credentials-SITE_ID
              key: WORDPRESS_DB_USER
        - name: WORDPRESS_DB_PASSWORD
          valueFrom:
            secretKeyRef:
              name: credentials-SITE_ID
              key: WORDPRESS_DB_PASSWORD
        - name: WORDPRESS_TABLE_PREFIX
          valueFrom:
            secretKeyRef:
              name: credentials-SITE_ID
              key: WORDPRESS_TABLE_PREFIX
        - name: APACHE_RUN_USER
          valueFrom:
            secretKeyRef:
              name: credentials-SITE_ID
              key: APACHE_RUN_USER
        - name: APACHE_RUN_GROUP
          valueFrom:
            secretKeyRef:
              name: credentials-SITE_ID
              key: APACHE_RUN_GROUP
        - name: GIT_USER
          valueFrom:
            secretKeyRef:
              name: credentials-SITE_ID
              key: GIT_USER
        - name: GIT_TOKEN
          valueFrom:
            secretKeyRef:
              name: credentials-SITE_ID
              key: GIT_TOKEN


        # Readiness probes help Kubernetes know when to start sending traffic to the pod.
        #   This probe helps kubernetes to "add" or "remove" this pod from the service in front of it.
        #   So, when the probe passes it is added to the service,
        #       when it fails at any time in it's life, kubernetes removes it from the service.
        # Restarting the "stuck" container is the job of livenessProbe, explained further down.
        # Note: Readiness probe runs throughout the life of the container - every "periodSeconds",
        #       not just at the container start time!
        #
        # The WITPASS/wordpress images takes long to start, 
        #   because themes and plugins are pulled at container start time.
        # 

        # Note:
        # gasheatinginstallers.co.uk uses a very weird theme, "listingpro"
        # This theme needs some plugins to be installed everytime the container starts.
        # The regular readiness probe does not work, because the main page has error.
        # So we get licence.txt, which is always there in wordpress installations. 

        readinessProbe:
          httpGet:
            path: /license.txt
            port: 80
          initialDelaySeconds: 5
          # Retry the probe every X seconds (frequency):
          periodSeconds: 5
          # Number of times this probe can fail ,
          #   before kubernetes gives up and marks the "pod" as "Unready":
          failureThreshold: 60
          # This sites takes a lot of time to boot up.
          # In the case above, the Pod will be marked "Unready",
          #   if the probe does not pass 5 x 60 = 300 seconds. = 5 minutes
          successThreshold: 1
          
        ports:
        - containerPort: 80
          name: http

        resources:
          requests:
            cpu: 100m
            memory: 192Mi
          limits:
            cpu: 200m
            memory: 256Mi

        volumeMounts:
        - name: volume-SITE_ID
          mountPath: /var/www/html/wp-content/uploads

      volumes:
      - name: volume-SITE_ID
        persistentVolumeClaim:
          claimName: pvc-SITE_ID

---

# We need service
apiVersion: v1
kind: Service
metadata:
  name: SITE_ID
  labels:
    app: SITE_ID
    tier: frontend
    apptype: wordpress
spec:
  ports:
    - port: 80
  selector:
    app: SITE_ID
    tier: frontend
    apptype: wordpress
  type: ClusterIP

---

# We need an ingress
# apiVersion: extensions/v1beta1
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: SITE_ID
  labels:
    app: SITE_ID
    tier: frontend
    apptype: wordpress
spec:
  rules:
  - host: SITE_NAME
    http:
      paths:
      - path: /
        backend:
          serviceName: SITE_ID
          servicePort: 80
  - host: www.SITE_NAME
    http:
      paths:
      - path: /
        backend:
          serviceName: SITE_ID
          servicePort: 80
