# Production Environment - Docker Host:

Suppose you want to deploy a wordpress website ( `http://example.com` ) on your local computer using `Docker`.

0. Install `docker` and `docker-compose` on your computer.
1. Since this is a local setup, add an entry in the hosts file of your OS (Linux: `/etc/hosts`). i.e. 


```
127.0.0.1    example.com
``` 

2. On your local development computer, make clone/copy of "this" wordpress-example-template repository, while using the name of website you want to use it for (i.e. `example.com` ) - inside a directory of your choice (e.g. /home/kamran/Projects/).

```
cd /home/kamran/Projects/
```

Using HTTPS:
```
git clone https://gitlab.com/wbitt/examples/example-wordpress-website.git  example.com
```

or

Using SSH:
```
git clone git@gitlab.com:wbitt/examples/example-wordpress-website.git  example.com
```


Then, remove the `.git` directory from inside it. 

```
cd example.com
rm -fr .git
```


3. On the local devpc, setup the correct directory structure for wordpress persistent storage:
```
mkdir -p  ~/tmp/example.com/wp-content

chmod 1777  ~/tmp/example.com/wp-content
```



4. The `docker-compose.devpc.yml` contains a simple setup for MySQL, so you really don't need to setup any special environment variables for it. It is setup with basic information.

4. Copy/rename `wordpress.devpc.env.example` file to `wordpress.devpc.env`, and adjust it as required.  


5. Adjust the following ENV variables in the `wordpress.devpc.env` file:

```
# You must always set TimeZone for your container.
# The format follows the directory layout in "/usr/share/zoneinfo/" on any linux system.
#
TZ=Europe/London


# This is important to set. 
# If this is skipped, NGINX_HOST will get default value of 'localhost'.
NGINX_HOST=example.com

# This is important to set. 
# If this is skipped, NGINX_PORT will get default value of '80'.
# In some cases you may want to run NGINX on a different port,
#   such as instances when you have a cache server (varnish) 
#   running in front of nginx .
# In that case set it to 8780 (ASCII code for W=87, P=80 -> WP=8780 )
# NGINX_PORT=8780
NGINX_PORT=80


# This variable defines the location of the nginx template file for default.conf
# If you don't specify this, it will be set to /etc/nginx/templates/default.conf.template
NGINX_CONFIG_TEMPLATE_FILE=/etc/nginx/templates/default.conf.template

# Set 'gzip' = 'on'|'off' in main nginx.conf. 
#   Turning it off reduced load on web server, and is better for the web-server.
# Default is 'off'.
NGINX_GZIP=off


# The following two variables are used by php-fpm container to run as this user.
#   If you don't specify this, then the default "www-data" with uid "33",
#   will be used by the php-fpm container.
# When on DEV PC, you should use the UID of your user instead for WEB_RUN_UID variable.

WEB_RUN_USERNAME=web
WEB_RUN_UID=1000

# The following is only used if you are using some private repositories
#   from your GIT provider.
GIT_USER=yourgit-provider-accountname
GIT_TOKEN=yourgit-provider-token

# The following DELETE_* variables are used to delete ALL themes,
#   and ALL plugins when set to true. This should be relatively 
#   harmless operation, because you should have themes and plugins
#   either part of this repo, or listed in the download*.list files.
# In worst case situation, the wordpress admin panel is always available,
#   which can be used to install a theme manually.
# Once happy with cleaning up, set these two variables back to "false",
#   and restart the docker-compose stack.

DELETE_ALL_THEMES=true
DELETE_ALL_PLUGINS=true
DELETE_WEB_DOCUMENT_ROOT=false
```

**Note:** You don't need to change any **`WORDPRESS_*`** variables in this file. 

6. While using this image, it is possible to pull themes and plugins from private repositories to which you have been given access to. In that case, you will need to generate a **git token** for your user account, (`Edit Profile -> Access Tokens`), and provide it in the `wordpress.devpc.env` file.

Add the following to the wordpress environment file.
```
GIT_USER=yourgit<provider>accountname
GIT_TOKEN=yourgit<provider>token
```

8. Adjust any plugins or themes you want in the following files:
  * `themes/download-themes.list`
  * `themes/gitrepos-themes.list`
  * `plugins/download-plugins.list`
  * `plugins/gitrepos-plugins.list`


9. **Optional step** - You can create this directory as a new git repository in gitlab/github/xyz web UI with the same name as your local directory. i.e. `example.com` . The web UI will give you instructions on how to create the repository on your local computer.

10. **Optional step** - On the local computer, using instructions from the previous step, setup your directory as a git repository using `git init`, `git remote add`, and `git add/commit/push` the changes to remote.

10. While in the `example.com` directory, execute the following commands to bring up your wordpress website.

```
docker-compose -f docker-compose.devpc.yml stop
docker-compose -f docker-compose.devpc.yml rm -f
docker-compose -f docker-compose.devpc.yml build --force-rm
docker-compose -f docker-compose.devpc.yml up -d
sleep 5
docker-compose -f docker-compose.devpc.yml ps
```

11. Access your website in a browser: `http://example.com`, and go through the Wordpress installation. Make a note of Admin username and password.


12. Later, if you make changes to plugins or themes, then you will need to restart the docker stack. 

You go into the `example.com` directory on your local computer, and run the above docker commands again.

```
cd /home/kamran/Projects/example.com

docker-compose -f docker-compose.devpc.yml stop
docker-compose -f docker-compose.devpc.yml rm -f
docker-compose -f docker-compose.devpc.yml build --force-rm
docker-compose -f docker-compose.devpc.yml up -d
sleep 5
docker-compose -f docker-compose.devpc.yml ps
```


------
# Appendices:

## Appendix A: Completely delete / refresh your WP installation:

Stop Wordpress docker stack:
```
docker-compose -f docker-compose.devpc.yml stop
docker-compose -f docker-compose.devpc.yml rm -f
```

Remove any files (plugins, themes, uploads)

```
rm -fr ~/tmp/example.com/
```


Find the docker volume being used by mysql, and delete it:

```
docker volume ls
```

```
docker volume rm examplecom_db
```


## Appendix B:

If you have a MySQL DB server/service running somewhere and you would prefer to use that instead, then use the following commands to create a wordpress database in mysql.

```
create database example_com_com;

grant all on example_com_com.* to 'example_com_com'@'%' identified by 'super-secret-db-password';

flush privileges;
```

```
MariaDB [(none)]> create database example_com_com;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> grant all on example_com_com.* to 'example_com_com'@'%' identified by 'super-secret-db-password';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> flush privileges;
Query OK, 0 rows affected (0.001 sec)
```



