

## Production Environment - Kubernetes Cluster:

Suppose you want to deploy a wordpress website (`https://blog.yourdomain.com`) on a Kubernetes Cluster.


1. Make sure the the DNS is setup correctly. i.e. `blog.yourdomain.com` resolves to a public IP address of your Ingress Controller / Reverse Proxy running for your cluster. 

If you don't have `dig` , you can use `nslookup`.

e.g. Check an existing domain already hosted in the Kubernetes cluster. 

```
[kamran@kworkhorse ~]$ dig +short gasheatinginstallers.co.uk
34.89.30.46
[kamran@kworkhorse ~]$ 
```

Check the IP of new domain you want to host:
```
[kamran@kworkhorse ~]$ dig +short blog.yourdomain.com
gcp-traefik.witpass.co.uk.
34.89.30.46
```

2. On your local development computer, make clone/copy of "this" wordpress-example-template repository as the name of website you want to use it for.

```
git clone https://gitlab.com/wbitt/examples/example-wordpress-website.git  blog.yourdomain.com
```

or

```
git clone git@gitlab.com:wbitt/examples/example-wordpress-website.git  blog.yourdomain.com
```

Then, remove the `.git` directory from inside it. If you want, you can also remove the `LICENSE.md` file.


```
cd blog.yourdomain.com
rm -fr .git
```

3. On the DB server, setup a new database for this wordpress application in such a way that the database has it's own "DB USER" and it's own "DB PASSWORD". **DO NOT use mysql `root` user's credentials in your wordpress DB**. 

**Note:** Make sure that you use underscore (_) instead of dots (.) in the dbname and db user. 

**Note:** For strong passwords use this:
```
openssl rand -base64 40
```


```
create database blog_yourdomain_com;

grant all on blog_yourdomain_com.* to 'blog_yourdomain_com'@'%' identified by 'super-secret-db-password';

flush privileges;
```


```
MariaDB [(none)]> create database blog_yourdomain_com;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> grant all on blog_yourdomain_com.* to 'blog_yourdomain_com'@'%' identified by 'super-secret-db-password';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> flush privileges;
Query OK, 0 rows affected (0.001 sec)
```

Add above information to the central passwords file (e.g. Google drive). 

Note down the values for the following variables, in a new file `wordpress.prod.env`:

```
WORDPRESS_DB_HOST=mysql.prod.svc.cluster.local
WORDPRESS_DB_NAME=blog_yourdomain_com
WORDPRESS_DB_USER=blog_yourdomain_com
WORDPRESS_DB_PASSWORD=super-secret-db-password
WORDPRESS_TABLE_PREFIX=wp_
```

Save these variables with their values in this file.

**Notes:** 
* Do not use quotes for the values in the `env` file. 
* **Do not use weak passwords**. Use this command to generate a relatively strong password: `openssl rand -base64 40` . Also make sure that you do not use '! @ $ % & | ? * ' in your passwords, as they have special meanings on Linux systems, and often it is painful to troubleshoot if the password has these characters. 
* Additionally, **verify that you can connect to this database manually** to avoid connection errors later. e.g. `mysql -h dbserver -u blog_yourdomain_com -D blog_yourdomain_com -p`


4. While using this image, it is possible to pull themes and plugins from private repositories to which you have been given access to. In that case, you will need to generate a **git token** for your user account, (`Edit Profile -> Access Tokens`), and provide it in the `wordpress.prod.env` file. 

Then, add the following to the wordpress environment file.
```
GIT_USER=yourgit<provider>accountname
GIT_TOKEN=yourgit<provider>token
```

6. You must always set Time Zone for your container. Use **`TZ`** environment variable to select the correct timezone. The format follows the directory layout in "/usr/share/zoneinfo/" on any Linux system. Add the following to your wordpress environment file.

```
TZ=Europe/London
```

7. The following DELETE_* variables are used to delete ALL themes, and ALL plugins when set to true. This should be relatively harmless operation, because you should have themes and plugins either part of this repo, or listed in the download*.list files. 

In worst case situation, the wordpress admin panel is always available, which can be used to install a theme manually. Once happy with cleaning up, set these variables back to "false", and redeploy the wordpress application.

```
DELETE_ALL_THEMES=false
DELETE_ALL_PLUGINS=false
```

The `DELETE_WEB_DOCUMENT_ROOT` variable removes the WP installation from document root. It is a harmless operation, because everytime the container starts, it copies the WP installation in DOCUMENT_ROOT.

```
DELETE_WEB_DOCUMENT_ROOT=false
```

You must always set TimeZone for your container.
The format follows the directory layout in "/usr/share/zoneinfo/" on any linux system.

```
TZ=Europe/London
```

If this is skipped, NGINX_HOST will get default value of 'localhost'.
```
NGINX_HOST=example.com
```

If this is skipped, NGINX_PORT will get default value of '80'. In some cases you may want to run NGINX on a different port, such as instances when you have a cache server (varnish) running in front of nginx. In that case set it to `8780` (ASCII code for W=87, P=80 -> WP=8780 )

```
# NGINX_PORT=8780
NGINX_PORT=80
```

This variable defines the location of the nginx template file for `default.conf` If you don't specify this, it will be set to `/etc/nginx/templates/default.conf.template`

```
NGINX_CONFIG_TEMPLATE_FILE=/etc/nginx/templates/default.conf.template
```

Set 'gzip' = 'on'|'off' in main `nginx.conf`. Turning it off reduced load on web server, and is better for the web-server.

```
# Default is 'off'.
NGINX_GZIP=off
```

The following two variables are used by php-fpm container to run as this user. If you don't specify this, then the default "www-data" with uid "33", will be used by the php-fpm container.
```
WEB_RUN_USERNAME=web
WEB_RUN_UID=1000
```

The following is only used if you are using some private repositories from your GIT provider. When creating the token, it is wise to allow on `read repository`  and `read registry` access for this token.

```
GIT_USER=yourgit-provider-accountname
GIT_TOKEN=yourgit-provider-token
```

8. Adjust any plugins or themes you want in the files `themes/download.list` and `plugins/download.list`


9. Create a new repository in gitlab/github/xyz web UI with the same name as your local directory. i.e. `blog.yourdomain.com` . The web UI will give you instructions on how to create the repository on your local computer. You will need to run these instructions at a later step.


10. While in the web UI, setup some environment variables under `Gitlab Settings -> CI/CD -> Environment variables`.
* `GCP_CREDENTIALS`:  Set it as **'type: File'** (not 'type: Variable') - contains the contents of json file, related to GCP service account. You should already have this somewhere on your local computer. You can also copy the contents of this variable from some existing wordpress repository you have on gitlab. 
* `SITE_CREDENTIALS`: Set it as **'type: File'** (not 'type: Variable') - contains the contents of the `wordpress.prod.env` file you created in the previous steps on your local computer.

11. Rename the file `gitlab-ci.k8s.yml.disabled` to `.gitlab-ci.yml`. 

**Note:** Do this from `git bash`, and *not* from (windows) file manager. Windows file manager messes up with the file extensions and does not perform the operation correctly.

```
mv gitlab-ci.k8s.yml.disabled   .gitlab-ci.yml
```

**Important:** Adjust necessary variables in the `.gitlab-ci.yaml` file of your local repository. These are:

* `NAMESPACE`: The kubernetes namespace where the site will be deployed.
* `STORAGE_SIZE`: The size of persistent volume used to hold `wp-content` of this site. The size is written as `<number>G`. e.g. `1G`. **Note: Single uppercase 'G'**

**Note:** Once a PVC is created with given storage size, it will not be re-created even if you change the size in this file. So, select proper size for wordpress website. (in giga-bytes). Remember, persistent storage costs money, so be careful.


12. The `.gitlab-ci.yml` file has necessary mechanism to create the PVC and then create the deployment in the correct namespace. All of this will happen automatically, once this file is part of the repository and is pushed to remote.

12.a. Adjust `custom-fpm.conf` and `custom-php.ini` files with necessary adjustments (if any), such as memory requirements. These should match the resources in the `site-deployment.template-bottom.yaml` file. (This step will be improved later, so you don't have to adjust the template files by hand. 

It is good to set memory to at least 256 MB for PHP, otherwise the site will not load properly and you might get "Too many redirects" error, on an empty white browser page.

```
$ grep -v \; custom-php.ini 

memory_limit = 512M

date.timezone = Europe/London

cgi.fix_pathinfo = 0
upload_max_filesize = 50M
max_execution_time = 300
```

The k8s deployment template file can be set to access a maximum of 512 MB:

```
        resources:
          requests:
            cpu: 100m
            memory: 192Mi
          limits:
            cpu: 300m
            memory: 512Mi
```



13. On the local computer, using instructions from the previous step (where you created a new repository on web UI), setup your directory as a git repository (**Push an existing folder**) - using `git init`, `git remote add`, and `git add/commit/push` the changes to remote. 

**Now, Check Pipelines of this repository on git web UI.**


14. After the CI job finishes successfully, you should be able to verify that the PVC exists, and that the secrets are created and a deployment, service and ingress are created. Also, you will verify that the website works through a browser.


15. If you want, you can manually re-create Kubernetes secret from the `wordpress.prod.env` file, using the `create-k8s-secret.sh` shell script included in this repository. It will delete and re-create the secret in k8s in correct namespace.

```
SITE_NAME=blog.yourdomain.com

NAMESPACE=test

SECRET_NAME=credentials-${SITE_NAME}

./create-k8s-secret.sh \
    <namespace> \
    <site-name> \
    <secret-name>  \
    <path-and-name-of-secrets-file_ENV_FILE>
```



16. Access your website: `https://blog.yourdomain.com`


17. Later, if you make changes to plugins or themes and push changes to the repository, then simply add/commit/push your changes, and the website will be re-deployed for you, while keeping the persistent data safe.

