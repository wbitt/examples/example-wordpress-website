# Production Environment - Docker Host:

Suppose you want to deploy a wordpress website (`https://blog.yourdomain.com`) on a Docker Host.

1. Make sure the the DNS is setup correctly. i.e. `blog.yourdomain.com` resolves to a public IP address of your Docker server. 

If you don't have `dig` , you can use `nslookup`.

e.g. Check an existing domain already hosted on your docker server. 

```
[kamran@kworkhorse ~]$ dig +short ukitforce.co.uk
server3.do.kbinfra.me.
139.59.169.47
[kamran@kworkhorse ~]$ 
```

Check the IP of new domain you want to host:
```
[kamran@kworkhorse ~]$ dig +short blog.yourdomain.com
server3.do.kbinfra.me.
139.59.169.47
```

2. On your local development computer, make clone/copy of "this" wordpress-example-template repository as the name of website you want to use it for.
```
git clone https://gitlab.com/wbitt/examples/example-wordpress-website.git  blog.yourdomain.com
```

or

```
git clone git@gitlab.com:wbitt/examples/example-wordpress-website.git  blog.yourdomain.com
```


Then, remove the `.git` directory from inside it. If you want, you can also remove the `LICENSE.md` file.

```
cd blog.yourdomain.com
rm -fr .git
```


3. On the db server, setup a new database for this wordpress application in such a way that the database has it's own "DB USER" and it's own "DB PASSWORD". **DO NOT use mysql `root` user's credentials**. Note down the values for the following variables:

**Note:** Make sure that you use underscore instead of dots in the dbname and db user.

**Note:** Use `openssl rand -base64 40` to create strong password.

```
create database blog_yourdomain_com;

grant all on blog_yourdomain_com.* to 'blog_yourdomain_com'@'%' identified by 'super-secret-db-password';

flush privileges;
```

```
MariaDB [(none)]> create database blog_yourdomain_com;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> grant all on blog_yourdomain_com.* to 'blog_yourdomain_com'@'%' identified by 'super-secret-db-password';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> flush privileges;
Query OK, 0 rows affected (0.001 sec)
```

Add above information to the central passwords file (e.g. Google drive). 

Note down the values for the following variables, in a new file `wordpress.prod.env` in this repository:

```
WORDPRESS_DB_HOST=mysql.prod.svc.cluster.local
WORDPRESS_DB_NAME=blog_yourdomain_com
WORDPRESS_DB_USER=blog_yourdomain_com
WORDPRESS_DB_PASSWORD=super-secret-db-password
WORDPRESS_TABLE_PREFIX=wp_
```

Save these variables with their values in the file.

**Notes:** 
* Do not use quotes for the values in the `env` file. 
* **Do not use weak passwords**. Use this command to generate a relatively strong password: `openssl rand -base64 40` . Also make sure that you do not use '! @ $ |' in your passwords, as they have special meanings on Linux systems, and often it is painful to troubleshoot if the password has these characters. 
* Additionally, **verify that you can connect to this database manually** to avoid connection errors later. `mysql -h mysql.prod.svc.cluster.local -u blog_yourdomain_com -D blog_yourdomain_com -p`



4. On the server, setup the correct directory structure on the docker host:
```
[deployer@dockerhost ~]$ mkdir /home/containers-{data,secrets}/blog.yourdomain.com

[deployer@dockerhost ~]$ mkdir /home/containers-data/blog.yourdomain.com/wp-content

[deployer@dockerhost ~]$ id
uid=900(deployer) gid=900(deployer) groups=900(deployer),899(docker)
```
**Note:** The directory `/home/containers-runtime/blog.yourdomain.com` is intentionally not created on the sevrer in the commands above, because it will automatically be created when will do a `git clone` later.

5. Adjust the following ENV variables:
```

# You must always set TimeZone for your container.
# The format follows the directory layout in "/usr/share/zoneinfo/" on any linux system.
#
TZ=Europe/London


# This is important to set. 
# If this is skipped, NGINX_HOST will get default value of 'localhost'.
NGINX_HOST=example.com

# This is important to set. 
# If this is skipped, NGINX_PORT will get default value of '80'.
# In some cases you may want to run NGINX on a different port,
#   such as instances when you have a cache server (varnish) 
#   running in front of nginx .
# In that case set it to 8780 (ASCII code for W=87, P=80 -> WP=8780 )
# NGINX_PORT=8780
NGINX_PORT=80


# This variable defines the location of the nginx template file for default.conf
# If you don't specify this, it will be set to /etc/nginx/templates/default.conf.template
NGINX_CONFIG_TEMPLATE_FILE=/etc/nginx/templates/default.conf.template

# Set 'gzip' = 'on'|'off' in main nginx.conf. 
#   Turning it off reduced load on web server, and is better for the web-server.
# Default is 'off'.
NGINX_GZIP=off


# The following two variables are used by php-fpm container to run as this user.
#   If you don't specify this, then the default "www-data" with uid "33",
#   will be used by the php-fpm container.

WEB_RUN_USERNAME=web
WEB_RUN_UID=1000

# The following is only used if you are using some private repositories
#   from your GIT provider.
GIT_USER=yourgit-provider-accountname
GIT_TOKEN=yourgit-provider-token

# The following DELETE_* variables are used to delete ALL themes,
#   and ALL plugins when set to true. This should be relatively 
#   harmless operation, because you should have themes and plugins
#   either part of this repo, or listed in the download*.list files.
# In worst case situation, the wordpress admin panel is always available,
#   which can be used to install a theme manually.
# Once happy with cleaning up, set these two variables back to "false",
#   and restart the docker-compose stack.

DELETE_ALL_THEMES=false
DELETE_ALL_PLUGINS=false
DELETE_WEB_DOCUMENT_ROOT=false
```

6. While using this image, it is possible to pull themes and plugins from private repositories to which you have been given access to. In that case, you will need to generate a **git token** for your user account, (`Edit Profile -> Access Tokens`), and provide it in the `wordpress.prod.env` file. 

Add the following to the wordpress environment file.
```
GIT_USER=yourgit<provider>accountname
GIT_TOKEN=yourgit<provider>token
```

7. Copy the `wordpress.prod.env` file to `/home/containers-secrets/blog.yourdomain.com/` on the server:
```
scp wordpress.prod.env deployer@dockerhost.server.fqdn:/home/containers-secrets/blog.yourdomain.com/
```

8. Adjust any plugins or themes you want in the files `themes/download.list` and `plugins/download.list`


9. Create a new repository in gitlab/github/xyz web UI with the same name as your local directory. i.e. `blog.yourdomain.com` . The web UI will give you instructions on how to create the repo on your local computer.

10. On the local computer, using instructions from the previous step, setup your directory as a git repository using `git init`, `git remote add`, and `git add/commit/push` the changes to remote.


11. Now clone this repository on the server:

```
git clone git@gitlab.com:yourgitaccount/blog.yourdomain.com.git
```

Or:

```
git clone https://gitlab.com/yourgitaccount/blog.yourdomain.com.git
```



10. Change into the directory of newly cloned repository, and execute the following commands to bring up your wordpress website.

```
docker-compose -f docker-compose.server.yml stop
docker-compose -f docker-compose.server.yml rm -f
docker-compose -f docker-compose.server.yml build --force-rm --pull
docker-compose -f docker-compose.server.yml up -d
docker-compose -f docker-compose.server.yml ps
```

11. Access your website: `https://blog.yourdomain.com`


12. Later, if you make changes to plugins or themes and push changes to the repo, then you go into this directory on the server, and do a git pull, and run the above docker commands again.

```
cd /home/containers-runtime/blog.yourdomain.com

git pull

docker-compose -f docker-compose.server.yml stop
docker-compose -f docker-compose.server.yml rm -f
docker-compose -f docker-compose.server.yml build --force-rm --pull
docker-compose -f docker-compose.server.yml up -d
docker-compose -f docker-compose.server.yml ps
```

