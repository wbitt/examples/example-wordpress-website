FROM wbitt/wordpress:5.9.3-php-8.1-nginx-fpm


# Example of adding more OS software and more PHP extensions:
#
# RUN apt-get update && apt-get -y install bzip2  libpng-dev
# RUN docker-php-ext-install mysqli gd pdo pdo_mysql
#
# RUN docker-php-ext-install bcmath calendar ctype curl dba dom enchant exif fileinfo filter ftp gd gettext gmp hash iconv imap interbase intl json ldap mbstring mcrypt mysqli oci8 odbc opcache pcntl pdo pdo_dblib pdo_firebird pdo_mysql pdo_oci pdo_odbc pdo_pgsql pdo_sqlite pgsql phar posix pspell readline recode reflection session shmop simplexml snmp soap sockets spl standard sysvmsg sysvsem sysvshm tidy tokenizer wddx xml xmlreader xmlrpc xmlwriter xsl zip


# Copy themes and plugins from the repository to a fixed place inside the container image.
# The themes and plugins will be processed by the our custom-wp-entrypoint.sh script,
#   when the container starts.
#
COPY themes /usr/src/themes/
COPY plugins /usr/src/plugins/


# The nginx-default.conf.template is already included in the main/parent image.
# If you have any special modifications in that file, 
#   then uncomment the following line.
# COPY nginx-default.conf.template /etc/nginx/templates/default.conf.template

# If you have any special settings in these two files,
#   then uncomment these two lines.
COPY custom-php.ini              /usr/local/etc/php/conf.d/
COPY custom-fpm.conf             /usr/local/etc/php-fpm.d/

